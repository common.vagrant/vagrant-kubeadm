```
scp vagrant@172.42.42.100:~/.kube/config ~/.kube/config
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add nginx https://helm.nginx.com/stable
helm repo add https://gitlab.com/common.helm/kube/raw/master/index.yaml
helm repo add myrepo  https://gitlab.com/common.helm/kube/raw/master/index.yaml
helm repo add myrepo  https://gitlab.com/common.helm/kube/raw/master/
helm repo add istio.io https://storage.googleapis.com/istio-release/releases/1.1.7/charts/
helm repo add banzaicloud-stable https://kubernetes-charts.banzaicloud.com/
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account tiller
helm install myrepo/local-pv
helm install myrepo/metallb-with-configmap --namespace kube-system
helm install myrepo/kube-dashboard --namespace kube-system
helm install stable/prometheus-operator --namespace monitoring --name=prometheus-operator
kubectl -n monitoring patch svc prometheus-operator-grafana --patch '{"spec":{"type": "LoadBalancer"}}'
kubectl -n monitoring patch svc prometheus-operator-prometheus --patch '{"spec":{"type": "LoadBalancer"}}'
kubectl -n monitoring patch svc prometheus-operator-alertmanager --patch '{"spec":{"type": "LoadBalancer"}}'
helm install istio.io/istio-init --namespace istio-system
kubectl create secret generic kiali --from-literal "username=admin" --from-literal "passphrase=admin" --namespace istio-system
helm install istio.io/istio --namespace istio-system --set kiali.enabled=true --set grafana.enabled=true --set sidecarInjectorWebhook.enabled=true --set "kiali.dashboard.grafanaURL=http://grafana:3000"
kubectl label namespace default istio-injection=enabled
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.1/samples/bookinfo/platform/kube/bookinfo.yaml --namespace default
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.1/samples/bookinfo/networking/bookinfo-gateway.yaml --namespace default
kubectl get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}' -n kube-system
kubectl get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].port}' -n kube-system
```
