# kubernetes
Kubernetes playground

# create cluster

On your host system

```
git clone https://gitlab.com/common.vagrant/vagrant-kubeadm.git
cd vagrant-provisioning
vagrant up
vagrant ssh kmaster
```

# Create dashboard

Login to kmaster

```
kubectl create -f https://gitlab.com/common.vagrant/vagrant-kubeadm/raw/master/dashboard/dashboard.yaml
kubectl create -f https://gitlab.com/common.vagrant/vagrant-kubeadm/raw/master/dashboard/sa_cluster_admin.yaml
```

# Login to dashboard

```
example: kubectl describe secret dashboard-admin-token-xz2xh -n kube-system
```

https://172.42.42.100:32323

# Helm install

Install helm client

```
curl -L https://git.io/get_helm.sh | bash
```

Install helm server (tiller)

```
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account tiller
```

# Basic User Authentication

on kmaster node

```
cat <<EOF > /etc/kubernetes/pki/users.csv
asd123,developer,developer
EOF
kubeadm config view > kubeadm.yaml
```

modify kubeadm.yaml

```
apiServer:
  extraArgs:
    authorization-mode: Node,RBAC
    basic-auth-file: /etc/kubernetes/pki/users.csv
...
```

apply new config, pods should redeploy

```
sudo kubeadm upgrade apply --config kubeadm.yaml
```

create rolebinding

```
cat <<EOF > rb.yaml
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: rb1
subjects:
- kind: User
  name: developer # Name is case sensitive
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
EOF
kubectl create -f rb.yaml
```

on kubectl client side create context and user

```
vi ~/.kube/config
---
contexts:
- context:
    cluster: kubernetes
    user: developer-basic-auth
  name: developer-basic-auth@kubernetes
---
users:
- name: developer-basic-auth
  user:
    username: developer
---
```

now it should work

```
kubectl get pods --context developer-basic-auth@kubernetes --username developer --password asd123
```

In case dashboard is used

```
kubectl patch deployments kubernetes-dashboard --type=json -p='[{"op": "add", "path": "/spec/template/spec/containers/0/args/1", "value":"--authentication-mode=basic"}]'
```